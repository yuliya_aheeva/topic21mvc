package by.training.homework20.servlets;

import by.training.homework20.dao.OrderProductDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SummaryOrderServletTest {

    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private OrderProductDao orderProductDao;

    @Test
    public void testServletGetPart() throws IOException {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);

        lenient().when(response.getWriter()).thenReturn(printWriter);
        when(session.getAttribute("consent")).thenReturn("on");
        when(session.getAttribute("userId")).thenReturn(21);
        when(orderProductDao.getTotalPrice(21)).thenReturn((double) 12);

        assertEquals(session.getAttribute("consent"), "on");
        assertEquals(session.getAttribute("userId"), 21);
        assertEquals(orderProductDao.getTotalPrice(21), 12.0);

        verify(response, never()).sendRedirect(any(String.class));
        verify(session,times(1)).getAttribute("userId");
        verify(orderProductDao,times(1)).getTotalPrice(21);
    }
}