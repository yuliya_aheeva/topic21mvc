package by.training.homework20.filter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServletFilterTest {

    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private FilterChain filterChain;
    @Mock
    private HttpSession session;

    @Test
    public void testFilter() throws IOException, ServletException {
        when(request.getSession()).thenReturn(session);
        when(request.getRequestURI()).thenReturn("someURI");

        assertEquals("someURI", request.getRequestURI());
        assertEquals(session, request.getSession());

        filterChain.doFilter(request, response);

        verify(request, times(1)).getSession();
        verify(request, times(1)).getRequestURI();
        verify(response, never()).sendRedirect("someURL");
    }
}