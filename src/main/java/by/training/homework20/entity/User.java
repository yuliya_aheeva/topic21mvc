package by.training.homework20.entity;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 42L;

    private long id;
    private String name;
    private String password;

    public User() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
