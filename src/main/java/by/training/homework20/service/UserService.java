package by.training.homework20.service;

import by.training.homework20.dao.UserDao;
import by.training.homework20.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    public User initializeUser(final String userName) {
        User user = new User();
        if (userDao.isUserExist(userName)) {
            user = userDao.getUserByName(userName);
        } else {
            user.setName(userName);
            userDao.add(user);
        }
        return user;
    }
}
