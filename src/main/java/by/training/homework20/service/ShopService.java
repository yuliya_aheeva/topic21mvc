package by.training.homework20.service;

import by.training.homework20.entity.Order;
import by.training.homework20.entity.Product;
import by.training.homework20.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ShopService {
    private final UserService userService;
    private final OrderService orderService;
    private final ProductService productService;
    private final OrderProductService orderProductService;

    public HttpSession initializeUser(HttpServletRequest request) {
        User user = userService.initializeUser(request.getParameter("userName"));
        HttpSession session = request.getSession();
        session.setAttribute("userName", user.getName());
        session.setAttribute("userId", user.getId());
        System.out.println(session.getAttribute("userName"));
        System.out.println(session.getAttribute("userId"));
        createOrder(session);
        setProductListAttribute(session);
        setChosenProductsAttribute(session);
        return session;
    }

    private void createOrder(HttpSession session) {
        Order order = orderService.createNewOrder((long) session.getAttribute("userId"));
        session.setAttribute("orderId", order.getId());
    }

    private void setProductListAttribute(HttpSession session) {
        String chosenProducts = productService.getChosenProducts(
                orderProductService.getOrderProductDao().getChosenList((long) session.getAttribute("userId")));
        session.setAttribute("chosenProducts", chosenProducts);
        List<Product> productList = productService.getProductsAsList();
        session.setAttribute("productList", productList);
    }

    private void setChosenProductsAttribute(HttpSession session) {
        String chosenProducts = productService.getChosenProducts(
                orderProductService.getOrderProductDao().getChosenList((long) session.getAttribute("userId")));
        session.setAttribute("chosenProducts", chosenProducts);
    }

    public void addProductToList(HttpServletRequest request) {
        Product product = new Product(request.getParameter("productOrder"));
            orderProductService.getOrderProductDao().add(
                    (long) request.getSession().getAttribute("orderId"), product.getId());
        setChosenProductsAttribute(request.getSession());
    }

    public void submitOrder(HttpServletRequest request) {
        long userId = (long) request.getSession().getAttribute("userId");
        orderService.getOrderDao().updateTotalPrice(userId);
        double totalPrice = orderProductService.getOrderProductDao().getTotalPrice(userId);
        request.getSession().setAttribute("totalPrice", totalPrice);

    }
}
