package by.training.homework20.service;

import by.training.homework20.dao.OrderDao;
import by.training.homework20.entity.Order;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Getter
public class OrderService {
    @Autowired
    private OrderDao orderDao;

    public Order createNewOrder(final Long userId) {
        Order order = new Order();
        order.setUserId(userId);
        orderDao.add(order);
        return order;
    }
}
