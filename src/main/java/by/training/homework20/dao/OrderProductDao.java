package by.training.homework20.dao;

import by.training.homework20.entity.Product;

import java.util.List;

public interface OrderProductDao {

    void add(long orderId, long productId);

    double getTotalPrice(long userId);

    List<Product> getChosenList(long userId);

}
