package by.training.homework20.dao;

import by.training.homework20.entity.User;

public interface UserDao {

    void add(User user);

    User getUserByName(String userName);

    boolean isUserExist(String userName);
}
