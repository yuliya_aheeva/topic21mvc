package by.training.homework20.controller;

import by.training.homework20.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/online-shop")
public class WelcomeController {
    @Autowired
    ShopService shopService;

    @GetMapping
    public String doGetWelcomePage() {
        return "welcome";
    }

    @PostMapping("/login")
    public String doPostWelcomePage(HttpServletRequest request) {
        if (request.getParameter("userName") != null && request.getParameter("userName").length() >= 1) {
            HttpSession session = shopService.initializeUser(request);
            if (request.getParameter("button") != null) {
                if (request.getParameter("consent") != null) {
                    session.setAttribute("consent", "on");
                    return "redirect:catalog";
                } else {
                    return "notAgree";
                }
            }
        }
        return "notFound";
    }

    @GetMapping("/catalog")
    public String doGetCatalogPage() {
        return "catalog";
    }

    @PostMapping("/catalog")
    public String doPostCatalogPage(HttpServletRequest request) {
        if (request.getParameter("submit") != null) {
            shopService.submitOrder(request);
            return "redirect:order";
        } else {
            shopService.addProductToList(request);
            return "redirect:catalog";
        }
    }

    @GetMapping("/order")
    public String doGetOrderPage() {
        return "order";
    }
}
